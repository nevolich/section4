// Section_5 - final

@IsTest
private class TypeTest {

    @IsTest
    public static void getAndSetContactTypeTest() {
        Type typeContact = new Type();
        typeContact.setContactType('ContactTypeTest');
        System.assertEquals('ContactTypeTest', typeContact.getContactType(), 'Incorrect contact type');
    }

    @IsTest
    public static void getAndSetAmountOfMoneyTest() {
        Type typeContact = new Type();
        typeContact.setAmountOfMoney(12345.05);
        System.assertEquals(12345.05, typeContact.getAmountOfMoney(), 'Incorrect contact type');
    }

}