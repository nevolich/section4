/**
 * Created by locadmin on 11.09.2019.
 */

@IsTest
private class ExampleTest {
    @IsTest
    public static void getNameTest() {
        Example ex = new Example('App', 'Dev');

        Test.startTest();
        System.assertEquals('App Dev', ex.getName());
        Test.stopTest();

    }
}