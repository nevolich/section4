// Section_5_5

public with sharing class ExpensiveAccount implements Profit {

    private Account account;

    public List<Profit> team = new List<Profit>();
    public List<ExpensiveContact> contactWrappers = new List<ExpensiveContact>();

    public ExpensiveAccount(Account acc) {
        this.account = acc;
        System.debug('ExpensiveAccount: Constructor with one Account parameter - OK');
    }

    public ExpensiveAccount(String name) {
        this.account = new Account(Name = name);
        insert account;
        System.debug('ExpensiveAccount: Constructor with parameters - OK');
    }

    public void add(Profit teamMember) {
        team.add(teamMember);
    }

    public Decimal getProfit() {
        Decimal sumProfit = 0;
        for (Profit prof : team) {
            sumProfit += prof.getProfit();
        }
        return sumProfit;
    }

    public void setParentAccount(Account acc) {
        try {
            account.ParentId = acc.Id;
            update account;
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}