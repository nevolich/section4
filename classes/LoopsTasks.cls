public class LoopsTasks {

  /**
   * Returns the 'Fizz','Buzz' or an original number as String using the following rules:
   * 1) return original number as String
   * 2) but if number multiples of three return 'Fizz'
   * 3) for the multiples of five return 'Buzz'
   * 4) for numbers which are multiples of both three and five return 'FizzBuzz'
   *
   * @param {number} num
   * @return {any}
   *
   * @example
   *   2 =>  '2'
   *   3 => 'Fizz'
   *   5 => 'Buzz'
   *   4 => '4'
   *  15 => 'FizzBuzz'
   *  20 => 'Buzz'
   *  21 => 'Fizz'
   *
   */
  public static String getFizzBuzz(Integer num) {

    String outString = '';

    Integer i = num;

    while (i > 3-1) {
      i -= 3;
    }

    if (i == 0) {
      outString += 'Fizz';
    }

    i = num;

    while (i > 5-1) {
      i -= 5;
    }

    if (i == 0) {
      outString += 'Buzz';
    }

    if (outString.length() == 0) {
      outString += String.valueOf(num);
    }

    /*
      if (Math.mod(num, 3) == 0) {
        outString += 'Fizz';
      }
      if (Math.mod(num, 5) == 0) {
        outString += 'Buzz';
      }
      if (outString.length() == 0) {
        outString += String.valueOf(num);
      }
    */

    return outString;
  }

  /**
   * Returns the factorial of the specified integer n.
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   1  => 1
   *   5  => 120
   *   10 => 3628800
   */
  public static Integer getFactorial(Integer num) {

    Integer factorial = 1;

    for (Integer i = 1; i <= num; i++) {
      factorial *= i;
    }

    return factorial;
  }

  /**
   * Returns the sum of integer numbers between n1 and n2 (inclusive).
   *
   * @param {number} n1
   * @param {number} n2
   * @return {number}
   *
   * @example:
   *   1,2   =>  3  ( = 1+2 )
   *   5,10  =>  45 ( = 5+6+7+8+9+10 )
   *   -1,1  =>  0  ( = -1 + 0 + 1 )
   */
  public static Integer getSumBetweenNumbers(Integer num1, Integer num2) {
    Integer sum = 0;

    for (Integer i = num1; i <= num2; i++ ) {
      sum += i;
    }

    return sum;
  }

  /**
   * Returns true, if a triangle can be built with the specified sides a,b,c and false in any other ways.
   *
   * @param {number} a
   * @param {number} b
   * @param {number} c
   * @return {bool}
   *
   * @example:
   *   1,2,3    =>  false
   *   3,4,5    =>  true
   *   10,1,1   =>  false
   *   10,10,10 =>  true
   */
  public static Boolean isTriangle(Integer a, Integer b, Integer c) {

    Boolean outIsTriangle = true;

    if ((a + b <= c) || (a + c <= b) || (b + c <= a)) {
      outIsTriangle = false;
    }

    return outIsTriangle;
  }

  /**
   * Returns true, if two specified axis-aligned rectangles overlap, otherwise false.
   * Each rectangle representing by Map<String, Integer> 
   *  {
   *     top: 5,
   *     left: 5,
   *     width: 20,
   *     height: 10
   *  }
   * 
   *  (5;5)
   *     -------------  
   *     |           | 
   *     |           |  height = 10
   *     ------------- 
   *        width=20    
   * 
   * NOTE: Please use canvas coordinate space (https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#The_grid),
   * it differs from Cartesian coordinate system.
   * 
   * @param {Map<String, Integer>} rect1
   * @param {Map<String, Integer>} rect2
   * @return {bool}
   *
   * @example:
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top: 5, left: 5, width: 20, height: 20 }    =>  true
   * 
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top:20, left:20, width: 20, height: 20 }    =>  false
   *  
   */
  public static Boolean doRectanglesOverlap(Map<String, Integer> rect1, Map<String, Integer> rect2) {

    Boolean isOverlapTop = false;
    Boolean isOverlapLeft = false;


    for (Integer i = rect1.get('top'); i <= rect1.get('top') + rect1.get('height'); i++) {
      for (Integer j = rect2.get('top'); j <= rect2.get('top') + rect2.get('height'); j++) {
        if (i == j) {
          isOverlapTop = true;
        }
      }
    }

    for (Integer i = rect1.get('left'); i <= rect1.get('left') + rect1.get('width'); i++) {
      for (Integer j = rect2.get('left'); j <= rect2.get('left') + rect2.get('width'); j++) {
        if (i == j) {
          isOverlapLeft = true;
        }
      }
    }

    return (isOverlapTop && isOverlapLeft);
  }

  /**
   * Returns true, if point lies inside the circle, otherwise false.
   * Circle is:
   *     Center Map<String, Double>: {
   *       x: 5,       
   *       y: 5
   *     },        
   *     Radius Integer: 20
   * Point is Map<String, Double> 
   *  {
   *     x: 5,
   *     y: 5
   *  }
   * 
   * @param {Map<String, Double>} center
   * @param {Integer} raduis
   * @param {Map<String, Double>} point
   * @return {bool}
   *
   * @example:
   *   center: { x:0, y:0 }, radius: 10, point: { x:0, y:0 }     => true
   *   center: { x:0, y:0 }, radius:10,  point: { x:10, y:10 }   => false
   *   
   */
  public static Boolean isInsideCircle(Map<String, Double> center, Integer radius, Map<String, Double> point) {

    Boolean outIsInsideCircle = false;

    if (radius > Math.sqrt( Math.pow(center.get('x') - point.get('x'), 2) + Math.pow(center.get('y') - point.get('y'), 2))) {
      outIsInsideCircle = true;
    }

    return outIsInsideCircle;
  }

  /**
   * Returns the first non repeated char in the specified strings otherwise returns null.
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   *   'The quick brown fox jumps over the lazy dog' => 'T'
   *   'abracadabra'  => 'c'
   *   'entente' => null
   */
  public static String findFirstSingleChar(String str) {

    //String singleChars = '';
    String ch;

    for (Integer i = 0; i < str.length(); i++) {
      ch = str.substring(i, i+1);

      if (str.indexOf(ch) == str.lastIndexOf(ch) ) {
        //singleChars += ch;
        return ch;
      }
    }

    return null;

    /*
    if (singleChars.length() == 0) {
      return null;
    } else {
      return singleChars.substring(0,1);
    }
    */
  }

  /**
   * Returns the string representation of math interval, specified by two points and include / exclude flags.
   * See the details: https://en.wikipedia.org/wiki/Interval_(mathematics)
   *
   * Please take attention, that the smaller number should be the first in the notation
   *
   * @param {number} a
   * @param {number} b
   * @param {bool} isStartIncluded
   * @param {bool} isEndIncluded
   * @return {string}
   *
   * @example
   *   0, 1, true, true   => '[0, 1]'
   *   0, 1, true, false  => '[0, 1)'
   *   0, 1, false, true  => '(0, 1]'
   *   0, 1, false, false => '(0, 1)'
   * Smaller number has to be first :
   *   5, 3, true, true   => '[3, 5]'
   *
   */
  public static String getIntervalString(Integer a, Integer b, Boolean isStartIncluded, Boolean isEndIncluded) {

    String str = '';

    if (isStartIncluded) {
      str += '[';
    } else {
      str += '(';
    }

    str += Math.min(a, b);

    str += ', ';

    str += Math.max(a, b);

    if (isEndIncluded) {
      str += ']';
    } else {
      str += ')';
    }

    return str;
  }

  /**
   * Reverse the specified string (put all chars in reverse order)
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   * 'The quick brown fox jumps over the lazy dog' => 'god yzal eht revo spmuj xof nworb kciuq ehT'
   * 'abracadabra' => 'arbadacarba'
   * 'rotator' => 'rotator'
   * 'noon' => 'noon'
   */
  public static String reverseString(String str) {

    String outStr = '';

    for (Integer i = str.length() - 1; i >= 0; i--) {
      outStr += str.substring(i, i+1);
    }

    return outStr;
    //return str.reverse();
  }

  /**
   * Reverse the specified integer number (put all digits in reverse order)
   *
   * @param {number} num
   * @return {number}
   *
   * @example:
   *   12345 => 54321
   *   1111  => 1111
   *   87354 => 45378
   *   34143 => 34143
   */
  public static Integer reverseInteger(Integer num) {

    Integer outInt = 0;

    while (num >= 10) {
      outInt = 10 * outInt + Math.mod(num, 10);
      num = num / 10;
    }

    outInt = 10 * outInt + num;

    return outInt;
  }

  /**
   * Returns the digital root of integer:
   *   step1 : find sum of all digits
   *   step2 : if sum > 9 then goto step1 otherwise return the sum
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   12345 ( 1+2+3+4+5 = 15, 1+5 = 6) => 6
   *   23456 ( 2+3+4+5+6 = 20, 2+0 = 2) => 2
   *   10000 ( 1+0+0+0+0 = 1 ) => 1
   *   165536 (1+6+5+5+3+6 = 26,  2+6 = 8) => 8
   */
  public static Integer getDigitalRoot(Integer num) {

    Integer sum = 0;

    do {
      sum = 0;

      while (num >= 1) {
        sum += Math.mod(num, 10);
        num = num / 10;
      }

      num = sum;
    } while (sum >= 10);

    return sum;
  }

  /**
   * Returns true if the specified string has the balanced brackets and false otherwise.
   * Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
   * (in that order), none of which mis-nest.
   * Brackets include [],(),{},<>
   *
   * @param {string} str
   * @return {boolean}
   *
   * @example:
   *   '' => true
   *   '[]'  => true
   *   '{}'  => true
   *   '()   => true
   *   '[[]' => false
   *   ']['  => false
   *   '[[][][[]]]' => true
   *   '[[][]][' => false
   *   '{)' = false
   *   '{[(<{[]}>)]}' = true 
   */
  public static Boolean isBracketsBalanced(String str) {

    Integer len = str.length();
    System.debug('START::: str=' + str);

    while (str.length() > 0) {
      len = str.length();
      str = str.replace('()','').replace('[]','').replace('{}','').replace('<>','');

      if (len == str.length()) {
        return false;
      }
    }

    return true;
  }

  /**
   * Returns the human readable string of time period specified by the start and end time.
   * The result string should be constrcuted using the folliwing rules:
   *
   * ---------------------------------------------------------------------
   *   Difference                 |  Result
   * ---------------------------------------------------------------------
   *    0 to 45 seconds           |  a few seconds ago
   *   45 to 90 seconds           |  a minute ago
   *   90 seconds to 45 minutes   |  2 minutes ago ... 45 minutes ago
   *   45 to 90 minutes           |  an hour ago
   *  90 minutes to 22 hours      |  2 hours ago ... 22 hours ago
   *  22 to 36 hours              |  a day ago
   *  36 hours to 25 days         |  2 days ago ... 25 days ago
   *  25 to 45 days               |  a month ago
   *  45 to 345 days              |  2 months ago ... 11 months ago
   *  345 to 545 days (1.5 years) |  a year ago
   *  546 days+                   |  2 years ago ... 20 years ago
   * ---------------------------------------------------------------------
   *
   * @param {DateTime} startDate
   * @param {DateTime} endDate
   * @return {string}
   *
   * @example
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:01')  => 'a few seconds ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:05')  => '5 minutes ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-02 03:00:05')  => 'a day ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2015-01-02 03:00:05')  => '15 years ago'
   *
   */
  public static String timespanToHumanString(DateTime startDate, DateTime endDate) {

    //split date
    Integer years = (endDate.year() - startDate.year());
    Integer month = (endDate.month() - startDate.month());
    Integer day = (endDate.day() - startDate.day());
    Integer hour = (endDate.hour() - startDate.hour());
    Integer minute = (endDate.minute() - startDate.minute());
    Integer second = (endDate.second() - startDate.second());

    //if hours in endDate is less than hours in startDate
    if (hour < 0) {hour += 24;}
    if (minute < 0) {minute += 24;}
    if (second < 0) {second += 24;}

    //debug
    System.debug('TIME :::: startDate=\'' + startDate + '\', endDate=\'' + endDate + '\'');
    System.debug('years=' + years);
    System.debug('month=' + month);
    System.debug('day=' + day);
    System.debug('hour=' + hour);
    System.debug('minute=' + minute);
    System.debug('second=' + second);

    //solve full days
    Integer d = (Date.newInstance(startDate.year(),startDate.month(),startDate.day())).daysBetween(Date.newInstance(endDate.year(),endDate.month(),endDate.day()));
    //solve all seconds
    Integer bigS = second + minute *60 + hour*60*60 + day*60*60*24 + month*60*60*24*30 + years*60*60*24*30*365;

    //debug
    System.debug('TIME :::: bigSeconds=' + bigS);
    System.debug('TIME :::: d=' + d);


    //logic
    if (d >= 546)                                       {return d/365 + ' years ago'; }
    if (345 <= d && d < 546)                            {return 'a year ago';}
    if ( 45 <  d && d < 345)                            {return (d+15)/30 +' months ago';}

    if ( 25 *60*60*24 < bigS && bigS <= 45 *60*60*24)   {return 'a month ago';}
    if ( 2 *60*60*24 <= bigS && bigS <= 25 *60*60*24)   {return d + ' days ago';}
        if ( 36 *60*60 < bigS)                          {return '2 days ago';}

    if (22 *60*60 < bigS && bigS <= 36 *60*60)          {return 'a day ago';}

    if (2 *60*60 < bigS && bigS <= 22 *60*60)           {return (bigS+30*60-1)/60/60 + ' hours ago';}
        if (90 *60 < bigS && bigS <= 2 *60*60)          {return '2 hours ago';}

    if (45 *60 < bigS && bigS <= 90 *60*60)             {return 'an hour ago';}

    if (90+30 < bigS && bigS <= 45 *60)                 {return bigS/60 + ' minutes ago';}
      if (90 < bigS && bigS <= 90+30)                   {return '2 minutes ago';}

    if (45 < bigS && bigS <= 90)                        {return 'a minute ago';}

    if (45 >= bigS)                                     {return 'a few seconds ago';}

    return null;
  }
}