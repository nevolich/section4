// Section_5 - final

@IsTest
private class AccountSwitcherImplTest {

    @IsTest
    public static void getAndSortTest() {
        // Generate test data
        createSomeContacts(10, 'PrivateContact', 25000);
        createSomeContacts(10, 'PublicContact', 5000);

        // Call method for test
        AccountSwitcherImpl.getAndSort();

        System.assertEquals(20, AccountSwitcherImpl.allContacts.size(), 'Invalid count of contacts');

        // Sort contacts into private contact and public contact
        List<PrivateContact> privateContacts = new List<PrivateContact>();
        List<PublicContact> publicContacts = new List<PublicContact>();

        for (Type item : AccountSwitcherImpl.allContacts) {
            if (item instanceof PrivateContact) {
                privateContacts.add((PrivateContact)item);
            } else if (item instanceof PublicContact)  {
                publicContacts.add((PublicContact)item);
            }
        }

        System.assertEquals(10, privateContacts.size(), 'Invalid count of private contacts');
        System.assertEquals(10, publicContacts.size(), 'Invalid count of public contacts');

        // Check the correctness of determining the type of contact depending on the amount
        for (PrivateContact item : privateContacts) {
            System.assert(item.getContactRecord().Amount__c > 10000);
        }

        for (PublicContact item : publicContacts) {
            System.assert(item.getContactRecord().Amount__c <= 10000);
        }

        for (PrivateContact item : privateContacts) {
            if (item.getContactType() == 'Premier') {
                System.assertEquals(true, checkSameDigits(item.getCardNumber()));
            } else if (item.getContactType() == 'Person') {
                System.assertEquals(false, checkSameDigits(item.getCardNumber()));
            }
        }
    }

    @IsTest
    public static void getRandomNumberTest() {
        Integer numb = AccountSwitcherImpl.getRandomNumber();
        System.assert(0 <= numb, 'Random number cannot be negative');
        System.assert(numb <= 999999, 'Random number cannot be greater than 999999');
    }

    @IsTest
    public static void getRandomEvenNumberTest() {
        Integer evenNumb = AccountSwitcherImpl.getRandomEvenNumber();
        System.assert(0 <= evenNumb, 'Random even number cannot be negative');
        System.assert(evenNumb <= 999998, 'Random even number cannot be greater than 999998');
        System.assertEquals(0, Math.mod(evenNumb, 2), 'Random even number cannot be greater than 999998');
    }

    @IsTest
    public static void containThreeSameDigitsTest() {
        System.assertEquals(true, AccountSwitcherImpl.containThreeSameDigits(111234));
        System.assertEquals(true, AccountSwitcherImpl.containThreeSameDigits(909090));
        System.assertEquals(false, AccountSwitcherImpl.containThreeSameDigits(123456));
    }

    @IsTest
    public static void switchAccountTest() {
        // Generate test data
        createSomeContacts(2, 'PrivateContact', 25000);
        createSomeContacts(1, 'PublicContact', 5000);

        // Call method for test
        AccountSwitcherImpl.getAndSort();

        // Sort contacts into private contact and public contact
        List<PrivateContact> privateContacts = new List<PrivateContact>();
        List<PublicContact> publicContacts = new List<PublicContact>();

        for (Type item : AccountSwitcherImpl.allContacts) {
            if (item instanceof PrivateContact) {
                privateContacts.add((PrivateContact)item);
            } else if (item instanceof PublicContact)  {
                publicContacts.add((PublicContact)item);
            }
        }

        privateContacts[0].setCardNumber(123456);
        privateContacts[0].setContactType('Person');
        privateContacts[1].setCardNumber(111222);
        privateContacts[1].setContactType('Premier');

        Id idPersonPrivateContact = privateContacts[0].getContactRecord().AccountId;
        Id idPremierPrivateContact = privateContacts[1].getContactRecord().AccountId;
        Id idPersonPublicContact = publicContacts[0].getContactRecord().AccountId;

        DataResult aDataResult = AccountSwitcherImpl.switchAccount();

        System.assertEquals(idPersonPublicContact, privateContacts[0].getContactRecord().AccountId);
        System.assertEquals(idPersonPrivateContact, publicContacts[0].getContactRecord().AccountId);
        System.assertEquals(idPremierPrivateContact, privateContacts[1].getContactRecord().AccountId);

        System.assertEquals(1, aDataResult.ChangedPrivateContacts.size());
        System.assertEquals(1, aDataResult.ChangedPublicContacts.size());
        System.assertEquals(1, aDataResult.NonChangedPrivateContacts.size());

        System.assertEquals('Person', aDataResult.ChangedPrivateContacts[0].getContactType());
        System.assertEquals('Person', aDataResult.ChangedPublicContacts[0].getContactType());
        System.assertEquals('Premier', aDataResult.NonChangedPrivateContacts[0].getContactType());

    }

    @IsTest
    public static void swapAccountIdsOfContactsTest() {
        // Create and save two accounts
        List<Account> accounts = new List<Account>();
        Account firstAccount = new Account(Name = 'firstAccountName');
        Account secondAccount = new Account(Name = 'secondAccountName');
        accounts.add(firstAccount);
        accounts.add(secondAccount);
        insert accounts;

        // Two contacts have accounts
        Contact firstContact = new Contact(LastName = 'firstContactLastName', AccountId = firstAccount.Id);
        Contact secondContact = new Contact(LastName = 'secondContactLastName', AccountId = secondAccount.Id);

        AccountSwitcherImpl.swapAccountIdsOfContacts(firstContact, secondContact);

        System.assertEquals('firstContactLastName', firstContact.LastName);
        System.assertEquals('secondContactLastName', secondContact.LastName);
        System.assertEquals(secondAccount.Id, firstContact.AccountId);
        System.assertEquals(firstAccount.Id, secondContact.AccountId);

        // One contact doesn't have an account
        Contact contactWithAccount = new Contact(LastName = 'contactWithAccountLastName', AccountId = firstAccount.Id);
        Contact contactWithNoAccount = new Contact(LastName = 'contactWithNoAccountLastName');

        AccountSwitcherImpl.swapAccountIdsOfContacts(contactWithAccount, contactWithNoAccount);
        System.assertEquals(null, contactWithAccount.AccountId);
        System.assertEquals(firstAccount.Id, contactWithNoAccount.AccountId);
    }

    public static void createSomeContacts(Integer howMany, String name, Integer amount) {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();

        for (Integer iterator = 0; iterator < howMany; iterator++) {
            accounts.add(new Account(Name = 'Account ' + iterator));
        }

        insert accounts;

        //accounts = [SELECT Id, Name FROM Account LIMIT 1000];

        for (Integer iterator = 0; iterator < howMany; iterator++) {
            contacts.add(new Contact(LastName = name + iterator,
                    AccountId = accounts.get(iterator).Id,
                    Amount__c = amount));
        }

        insert contacts;
    }

    public static Boolean checkSameDigits(Integer numb) {

        Integer[] countOfDigits = new Integer[] {0,0,0,0,0,0,0,0,0,0};
        Boolean isContain = false;

        while (numb >= 1) {
            Integer digit = Math.mod(numb, 10);
            countOfDigits[digit]++;
            numb = numb / 10;
        }

        for (Integer item : countOfDigits) {
            if (item >= 3) {
                isContain = true;
            }
        }

        return isContain;
    }

}