// Exception
public class Section_5_1 {

/*
    1)Написать метод класса, в котором будут несколько блоков try,
    вкаждом из которых создаются и обрабатываются исключенияследующих типов:
    sObject exception, DML exception, List exception,NullPointer exeption.

    2)Написать метод класса. В нем будет блок try, в котором надосгенерировать DML exception и обработать его,
    в блоке catch нужно использовать все дополнительные методы исключения, уникальные для DML exception.

    3) Написать метод класса, который принимает Аккаунты. Если у нихполе ​NumberOfEmployees не заполнено,
    то генерироватьпользовательское исключение и обработать его.
*/

    public static void task_1() {

        // sObject exception
        try {
            Contact firstContact = [SELECT Id, Name FROM Contact LIMIT 1];
            String title = firstContact.Title;

        } catch (SObjectException e) {
            System.debug('Our SObjectException caught: ' + e.getMessage());
        }

        // DML exception
        try {
            Account accountWithoutName = new Account(AccountNumber = 'NZT', Phone = '123456789');
            insert accountWithoutName;

        } catch (DmlException e) {
            System.debug('Our DmlException caught: ' + e.getMessage());
        }

        // List exception
        try {
            List<Integer> integers = new List<Integer>();
            integers.add(100);
            Integer num = integers[2];

        } catch (ListException e) {
            System.debug('Our ListException caught: ' + e.getMessage());
        }

        // NullPointer exeption
        try {
            String emptyString;
            Boolean result = emptyString.contains('a');

        } catch (NullPointerException e) {
            System.debug('Our NullPointerException caught: ' + e.getMessage());
        }

    }


    /*
    2)Написать метод класса. В нем будет блок try, в котором надосгенерировать DML exception и обработать его,
    в блоке catch нужно использовать все дополнительные методы исключения, уникальные для DML exception.
    */
    public static void task_2() {

        // DML exception
        try {
            Account accountWithName = new Account(Name = 'Johnny', AccountNumber = 'XZ222', Phone = '1111111100');
            Account accountWithoutName = new Account(AccountNumber = 'AA0000', Phone = '480032100');
            Account accountWithNothing = new Account();

            List<Account> accounts = new List<Account>();
            accounts.add(accountWithName);
            accounts.add(accountWithoutName);
            accounts.add(accountWithNothing);

            insert accounts;

        } catch (DmlException e) {
            System.debug('Our DmlException caught: ' + e.getMessage());

            Integer numErrors = e.getNumDml();
            System.debug('number of errors (getNumDml): ' + numErrors);

            for (Integer index = 0; index < numErrors; index++) {
                System.debug('Error record #' + index);
                System.debug('getDmlIndex: ' + e.getDmlIndex(index));
                System.debug('getDmlFieldNames: ' + e.getDmlFieldNames(index));
                System.debug('getDmlId: ' + e.getDmlId(index));
                System.debug('getDmlStatusCode: ' + e.getDmlStatusCode(index));
                System.debug('getDmlType: ' + e.getDmlType(index));
            }
        }
    }


    /*
    3) Написать метод класса, который принимает Аккаунты. Если у нихполе ​NumberOfEmployees не заполнено,
    то генерироватьпользовательское исключение и обработать его.
    */
    public static void task_3() {
        List<Account> accounts = [SELECT Id, Name, NumberOfEmployees FROM Account LIMIT 20];

        for (Account checkAccount : accounts) {
            checkNumberOfEmployees(checkAccount);
        }
    }


    // custom Exception class for task 3
    public class NumberOfEmployeesException extends Exception {}


    // method gor task 3
    public static void checkNumberOfEmployees(Account receiveAccount) {
        try {
            if ( receiveAccount.NumberOfEmployees != NULL ) {
                System.debug('Account \'' + receiveAccount.Name + '\' have ' + receiveAccount.NumberOfEmployees + ' employees');
            } else {
                throw new NumberOfEmployeesException('Number Of Employees is empty!');
            }
        } catch (Exception e) {
            System.debug('Our Exception caught on \'' + receiveAccount.Name + '\': ' + e.getMessage());
        }

    }

}