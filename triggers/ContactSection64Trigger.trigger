/**
 * Created by locadmin on 09.10.2019.
 */

trigger ContactSection64Trigger on Contact (after insert) {

    System.debug('Trigger ContactSection64Trigger');

    if (Trigger.isAfter && Trigger.isInsert) {
        ContactSection64TriggerHandler.createContact(Trigger.new);
    }

}