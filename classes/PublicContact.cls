// Section_5 - final

public class PublicContact extends Type {

    private Integer volunteerNumber;
    private Contact contactRecord;

    public Integer getVolunteerNumber() {
        return volunteerNumber;
    }

    public void setVolunteerNumber(Integer value) {
        volunteerNumber = value;
    }

    public Contact getContactRecord() {
        return contactRecord;
    }

    public void setContactRecord(Contact value) {
        contactRecord = value;
    }

}