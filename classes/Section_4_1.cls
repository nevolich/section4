// DML - Section 4.1
public class Section_4_1 {

    ////////////////
    // 1
    public static void task_1() {

        List<Contact> userList = new List<Contact>();

        for (Integer i = 0; i < 5; i++) {
            Contact newContact = new Contact(FirstName = 'John', LastName = 'Doe' + i, Title = 'User');
            userList.add(newContact);
        }
        insert userList;
    }

    ////////////////
    // 2
    public static void task_2() {

        List<Contact> devList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
        Contact newContact = new Contact(FirstName = 'Bill', LastName = 'Gates' + i, Title = 'Developer');
        devList.add(newContact);
        }

        List<Contact> userList = [SELECT Id, FirstName, LastName, Title FROM Contact WHERE FirstName = 'John' and LastName like 'Doe%'];
        System.debug(userList.size());
        for (Integer i = 0; i < userList.size(); i++) {
        userList[i].Title = 'Developer';
        }

        List<Contact> allContactList = new List<Contact>();
        allContactList.addAll(userList);
        allContactList.addAll(devList);

        upsert allContactList;
    }

    /////////////
    // 3
    public static void task_3() {

        List<Contact> userList = [SELECT Id, FirstName, LastName, Title FROM Contact WHERE (FirstName = 'John' and LastName like 'Doe%') or (FirstName = 'Bill' and LastName like 'Gates%')];
        System.debug(userList.size());

        delete userList;
    }

    /////////////
    // 4
    public static void task_4() {

        List<Contact> userList = [SELECT Id, FirstName, LastName, Title FROM Contact WHERE (FirstName = 'John' and LastName like 'Doe%') or (FirstName = 'Bill' and LastName like 'Gates%') ALL ROWS];
        System.debug(userList.size());

        undelete userList;

    }

    /////////////
    // 5
    public static void task_5() {

        List<Contact> userList = new List<Contact>();

        for (Integer i = 0; i < 10; i++) {
            Contact newContact = new Contact(FirstName = 'Billy' + i, LastName = 'McDuck' + i, Title = 'User');
            userList.add(newContact);
        }
        for (Integer i = 10; i < 20; i++) {
            Contact newContact = new Contact(FirstName = 'Billy' + i, Title = 'User');
            userList.add(newContact);
        }
        System.debug('userList.size() after create: ' + userList.size());


        Savepoint savepointBeforeInsert = Database.setSavepoint();
        System.debug('Create savepoint before insert');

        Database.insert(userList, false);
        //Database.insert(userList, true);
        //Error on line 16, column 1: System.DmlException: Insert failed. First exception on row 10; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [LastName]: [LastName]
        //AnonymousBlock: line 16, column 1


        List<Contact> existUserList = [SELECT Id, FirstName, LastName, Title FROM Contact WHERE FirstName like 'Billy%'];
        System.debug('existUserList.size() after insert: ' + existUserList.size());

        if (existUserList.size() != userList.size()) {
            Database.rollback(savepointBeforeInsert);
            System.debug('Rollback to savepoint before insert');
        }

        existUserList = [SELECT Id, FirstName, LastName, Title FROM Contact WHERE FirstName like 'Billy%'];
        System.debug('existUserList.size() after rollback: ' + existUserList.size());

    }


    /////////////
    // 6
    public static void task_6() {

        System.debug(UserInfo.getFirstName());
        System.debug(UserInfo.getLastName());

        System.debug(UserInfo.getUserId());
        System.debug(UserInfo.getDefaultCurrency());
        System.debug(UserInfo.getUserEmail());
        System.debug(UserInfo.getOrganizationName());
        System.debug(System.today());
        System.debug(Limits.getLimitQueries());

    }

    public Integer myNumber = 3210123;

    public Integer intLimit {get; set;}

    public String firstName {get;set;}
    public String lastName {get;set;}
    public Id userId {get;set;}
    public String defaultCurrency {get;set;}
    public String userEmail {get;set;}
    public String organizationName {get;set;}
    public Date today {get;set;}
    public Integer limitQueries {get;set;}
/*
    public Boolean showCountdown {
        get {
            return currentContact != NULL && currentContact.Active_Student_Program__c != NULL ? currentContact.Active_Student_Program__r.Start_Date__c > system.today() : true;
        }
        set;
    }
   */

    public Section_4_1() {
        firstName = UserInfo.getFirstName();
        lastName = UserInfo.getLastName();
        userId = UserInfo.getUserId();
        defaultCurrency = UserInfo.getDefaultCurrency();
        userEmail = UserInfo.getUserEmail();
        organizationName = UserInfo.getOrganizationName();
        today = System.today();
        limitQueries = Limits.getLimitQueries();
    }

}