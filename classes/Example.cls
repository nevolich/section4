public class Example {
    private String name;
    
    public Example(String firstName, String lastName) {
        this.name = firstName + ' ' + lastName;
    }
    
    public String getName() {
        return this.name;
    }
}