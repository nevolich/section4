/*
public class Example1 {
	public List<String> method() {
        List<SObject> objs;
        objs = [select id, name, email, phone from contact];
        	List<String> strings;
        	integer size = objs.size();
        for(integer i = 0; i < size; i ++) {
            	boolean f = false;
            integer sizeNew = strings.size();
                for(integer j = 0; j < sizeNew; j ++) {
                if(objs[i].get('name') == strings[j]) {
                f = true;
                }
                }
                if(f != true) {
             strings.add(string.valueof(objs[i].get('name')));
          }
        }
        return strings;
    		}
}
*/

public class Example1 {

    public List<String> getUniqueContactNames() {
        List<Contact> existContacts;
        List<String> uniqueContactNames;

        existContacts = [SELECT Id, Name FROM Contact];

        for (Contact aContact : existContacts) {
            Boolean isUniqueName = false;

            for (String name : uniqueContactNames) {
                if (aContact.get('name') == name) {
                    isUniqueName = true;
                }
            }

            if (!isUniqueName) {
                uniqueContactNames.add(String.valueOf(aContact.get('name')));
            }
        }

        return uniqueContactNames;
    }

}
