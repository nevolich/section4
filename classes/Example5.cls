/*
public class Example5 {
    public static void func() {
        list<contact> list1 = [select id,name,phone, email from contact];
        list<contact> list1new = new list<contact>();
        for(contact c: list1)
        {
            Boolean f = false;
            integer i = 0;
            for(i=0;i<list1new.size();i++)
            {
                if(c.Name == list1new[i].Name){f=true;}

            }
            if(f!=true)
            {
                list1new.add(c);
            }
        }
        delete list1new;
        list<lead> list2 = [select id,name,phone, email,title from Lead];
        list<lead> list2new = new list<lead>();
        for(lead c: list2)
        {
            Boolean f = false;
            integer i = 0;
            for(i=0;i<list2new.size();i++)
            {
                if(c.Name == list2new[i].Name){f=true;}

            }
            if(f!=true)
            {
                list2new.add(c);
            }
        }
        delete list2new;
    }
}*/

public class Example5 {
    /*
    * Delete every first contact in group of contacts with the same name
    *
    */
    public static void deleteUnnecessaryContacts() {
        // mjymjuymjy
        List<Contact> contacts = [SELECT Id, Name FROM Contact]; // dvfdvfdvf
        List<Contact> firstContacts = new List<Contact>();

        /* jhjgjhgjmjgmjggmj */
/*  commented for CR-18
        for (Contact aContact : contacts) {
            Boolean isFirstContact = false;

            for (Contact contact : firstContacts) {
                if (aContact.Name == contact.Name) {
                    isFirstContact = true;
                    continue;
                }
            }

            if (!isFirstContact) {
                firstContacts.add(aContact);
            }
        }

*/  //end
        delete firstContacts;
    }

    /*
     *
     */

    /*
    * Delete every first lead in group of leads with the same name
    */
    public static void deleteUnnecessaryLeads() {
        List<Lead> existLeads = [SELECT Id, Name FROM Lead];
        List<Lead> firstLeadsFromGroups = new List<Lead>();

        for (Lead aLead: existLeads) {
            Boolean isFirst = false;

            for (Lead lead: firstLeadsFromGroups) {
                if (aLead.Name == lead.Name) {
                    isFirst = true;
                    continue;
                }
            }

            if (!isFirst) {
                firstLeadsFromGroups.add(aLead);
            }
        }

        delete firstLeadsFromGroups;
    }
}