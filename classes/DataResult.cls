/**
 * Created by locadmin on 30.09.2019.
 */

public class DataResult {

    public List<PrivateContact> ChangedPrivateContacts;
    public List<PublicContact> ChangedPublicContacts;
    public List<PrivateContact> NonChangedPrivateContacts;

    public DataResult() {
        ChangedPrivateContacts = new List<PrivateContact>();
        ChangedPublicContacts = new List<PublicContact>();
        NonChangedPrivateContacts = new List<PrivateContact>();
    }

}