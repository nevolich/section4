// Section_5 - final

public class PrivateContact extends Type {

    private Integer cardNumber;
    private Contact contactRecord;

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Integer value) {
        cardNumber = value;
    }

    public Contact getContactRecord() {
        return contactRecord;
    }

    public void setContactRecord(Contact value) {
        contactRecord = value;
    }

}