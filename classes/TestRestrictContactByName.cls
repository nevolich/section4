// Section_5_2 - Apex Tests
// Trailhead "Apex Testing"

@IsTest
private class TestRestrictContactByName {
    @IsTest
    static void testBehaviorBeforeInsert() {
        // Test data setup
        // Create an contact with lastname 'INVALIDNAME', and then try to insert it
        Contact contactWithInvalidLastName = new Contact(LastName='INVALIDNAME');

        // Perform test
        Test.startTest();
        Database.SaveResult insertResult = Database.insert(contactWithInvalidLastName, false);
        Test.stopTest();

        // Verify
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!insertResult.isSuccess());
        System.assert(insertResult.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML',
                insertResult.getErrors()[0].getMessage());

    }

    @IsTest
    static void testBehaviorBeforeUpdate() {
        // Test data setup
        // Create an contact with lastname 'INVALIDNAME', and then try to update it
        Contact contactWithNormalLastName = new Contact(LastName='Test Contact');
        insert contactWithNormalLastName;
        contactWithNormalLastName.LastName = 'INVALIDNAME';

        // Perform test
        Test.startTest();
        Database.SaveResult updateResult = Database.update(contactWithNormalLastName, false);
        Test.stopTest();

        // Verify
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!updateResult.isSuccess());
        System.assert(updateResult.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML',
        updateResult.getErrors()[0].getMessage());
    }
}