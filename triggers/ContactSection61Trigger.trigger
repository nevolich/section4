/**
 * Created by locadmin on 03.10.2019.
 */

// Task 1
/*
trigger ContactSection61Trigger on Contact (after insert) {

    System.debug('Trigger ContactTriggerSection611');

    if (Trigger.isAfter && Trigger.isInsert) {
        ContactSection61TriggerHandler.handleAfterInsert611(Trigger.new);
    }

}
*/

// Task 2
/*
trigger ContactSection61Trigger on Contact (before insert, before update, after insert) {

    System.debug('Trigger ContactTriggerSection612');

    if (Trigger.isAfter && Trigger.isInsert) {
        ContactSection61TriggerHandler.handleAfterInsert612(Trigger.new);
    } if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        System.debug(':::::: Trigger : Trigger.new before handler :::::: ' + Trigger.new );
        ContactSection61TriggerHandler.handleBeforeInsertUpdate612(Trigger.new);
        System.debug(':::::: Trigger : Trigger.new after handler  :::::: ' + Trigger.new );/
    }


// Task 3

trigger ContactSection61Trigger on Contact (before insert, before update, after insert) {

    System.debug('Trigger ContactTriggerSection613');

    if (Trigger.isAfter && Trigger.isInsert) {
        ContactSection61TriggerHandler.handleAfterInsert613(Trigger.new);
    } if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        ContactSection61TriggerHandler.handleBeforeInsertUpdate612(Trigger.new);
    }
*/

// Task 4

trigger ContactSection61Trigger on Contact (before insert, before update, after insert) {

    System.debug('Trigger ContactTriggerSection614');

    if (Trigger.isAfter && Trigger.isInsert) {
        ContactSection61TriggerHandler.handleAfterInsert614(Trigger.new);
    } if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        ContactSection61TriggerHandler.handleBeforeInsertUpdate612(Trigger.new);
    }

}