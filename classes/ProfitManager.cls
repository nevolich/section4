// Section_5_5

public with sharing class ProfitManager {
    private List<Profit> allMember;

    public ProfitManager(List<Profit> members) {
        allMember = members;
    }

    public Decimal calculateProfit() {
        Decimal sumProfit = 0;

        for (Profit member : allMember) {
            sumProfit += member.getProfit();
        }

        return sumProfit;
    }

    public static List<Profit> getContacts() {
        List<Profit> expensiveContacts = new List<Profit>();

        List<Contact> contacts = [SELECT Id, Name, LastName, Profit__c, AccountId FROM Contact WHERE LastName LIKE 'Expensive%'];

        for (Contact cont : contacts) {
            expensiveContacts.add(new ExpensiveContact(cont));
        }

        return expensiveContacts;
    }

    public static List<Profit> getAccounts() {
        List<Profit> expensiveAccounts = new List<Profit>();
        Set<Id> accountIds = new Set<Id>();

        List<Contact> contacts = [SELECT Id, Name, LastName, Profit__c, AccountId FROM Contact WHERE LastName LIKE 'Expensive%'];

        for (Contact cont : contacts) {
            accountIds.add(cont.AccountId);
        }

        List<Account> accounts = [SELECT Id, Name, ParentId FROM Account WHERE Id IN :accountIds];

        for (Account acc : accounts) {
            ExpensiveAccount expAccount = new ExpensiveAccount(acc);

            for (Contact cont : contacts) {
                if (acc.Id == cont.AccountId) {
                    expAccount.add(new ExpensiveContact(cont));
                }
            }

            expensiveAccounts.add(expAccount);
        }

        return expensiveAccounts;
    }
}