// Section_5_5

public with sharing class ExpensiveContact implements Profit {

    private Contact contact;

    public ExpensiveContact(Contact cont) {
        this.contact = cont;
        System.debug('ExpensiveContact: Constructor with one Contact parameter - OK' + ' Profit: ' + cont.Profit__c);
    }

    public ExpensiveContact(String lastName, Decimal profit) {
        this.contact = new Contact(LastName = lastName, Profit__c = profit);
        insert contact;
        System.debug('ExpensiveContact: Constructor with parameters - OK');
    }

    public Decimal getProfit() {
        if ( contact.Profit__c != null ) {
            return contact.Profit__c;
        } else {
            return 0;
        }
    }

    public void setProfit(Decimal value) {
        try {
            contact.Profit__c = value;
            update contact;
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

    public void setParentAccount(Account acc) {
        try {
            contact.AccountId = acc.Id;
            update contact;
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}