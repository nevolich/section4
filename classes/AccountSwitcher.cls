// Section_5 - final


public interface AccountSwitcher {
    void getAndSort();
    DataResult switchAccount();
}