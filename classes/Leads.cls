// Section_5_5
//delete this class

public with sharing class Leads {
    // First a no-argument constructor
    public Leads () {
        System.debug('1');
    }

    // A constructor with one argument
    public Leads (Boolean call) {
        System.debug('2');
    }

    // A constructor with two arguments
    public Leads (String email, Boolean call) {
        System.debug('3');
    }

    // Though this constructor has the same arguments as the
    // one above, they are in a different order, so this is legal
    public Leads (Boolean call, String email) {
        System.debug('4');
    }

    public Leads (Decimal value) {
        System.debug('5');
    }
}