@isTest
public class CrowCalcTest {

    // Задание 3: @testSetup
    // Используйте @testSetup для создания тестовых данных
    @TestSetup
    static void setup() {
        CrowCalcTest.createSomeCrows(10);
    }


    @isTest
    public static void testAddCrows() {

        Boolean addCrowsResult = CrowCalc.addCrows(10);

        System.assertEquals( 20, [SELECT COUNT() FROM Crow__c], 'Wrong answer!');
        System.assert(addCrowsResult, 'Success addition not "true"');

    }

    @isTest
    public static void testAddCrowsWithUndefinedCount() {
        // negative
        Integer undefinedCount = null;
        Boolean addCrowsWithUndefinedCountResult = CrowCalc.addCrows(undefinedCount);
        System.assert(addCrowsWithUndefinedCountResult == false, 'Unsuccess addition not "false"');
    }

    @isTest
    public static void testSubtractCrows() {
        CrowCalcTest.createSomeCrows(10);

        Integer countCrowsToDelete = 5;

        Integer crowsBefore = [SELECT COUNT() FROM Crow__c];

        Boolean subtractCrowsResult = CrowCalc.SubtractCrows(countCrowsToDelete);

        Integer crowsAfter = [SELECT COUNT() FROM Crow__c];

        System.assert(crowsAfter == crowsBefore - countCrowsToDelete, 'Wrong answer!');
        System.assert(subtractCrowsResult, 'Success subtract not "true"');

        Integer undefinedCount = null;
        Boolean addSubtractWithUndefinedCountResult = CrowCalc.subtractCrows(undefinedCount);
        System.assert(addSubtractWithUndefinedCountResult != true, 'Unsuccess subtract not "false"');
    }

    @isTest
    public static void testSubtractGreaterCrowsThenExist() {

        CrowCalcTest.createSomeCrows(10);

        Integer crowsBefore = [SELECT COUNT() FROM Crow__c];

        Integer countCrowsToDelete = crowsBefore + 5;

        Boolean subtractCrowsResult = CrowCalc.SubtractCrows(countCrowsToDelete);

        Integer crowsAfter = [SELECT COUNT() FROM Crow__c];

        System.assertEquals(0, crowsAfter, 'Wrong answer!');
        System.assert(subtractCrowsResult, 'Success subtract not "true"');
    }

    @isTest
    public static void testGetTotal() {
        Integer existsCrows = [SELECT COUNT() FROM Crow__c];

        Integer totalCrows = CrowCalc.getTotal();

        System.assertEquals(existsCrows, totalCrows, 'Wrong answer!');
    }

    @isTest
    public static void testResetCalc() {
        CrowCalcTest.createSomeCrows(10);

        Integer crowsBefore = [SELECT COUNT() FROM Crow__c];

        Boolean resetCrowsReset = CrowCalc.resetCalc();

        Integer crowsAfter = [SELECT COUNT() FROM Crow__c];

        System.assert(crowsAfter == 0, 'Wrong answer!');
        System.assert(resetCrowsReset, 'Success reset not "true"');
    }


    @isTest
    public static void testAddBigCrows() {
        Integer crowsBefore = [SELECT COUNT() FROM Crow__c];

        Integer countCrowsToAdd = Limits.getLimitDmlRows() - crowsBefore;
        Boolean addCrowsResult = CrowCalc.addCrows(countCrowsToAdd);

        Integer crowsAfter = [SELECT COUNT() FROM Crow__c];

        System.assert(crowsAfter == crowsBefore + countCrowsToAdd, 'Wrong answer!');
        System.assert(addCrowsResult, 'Success addition not "true"');



    }


    @isTest
    public static void testAddBigCrowsGreaterThanLimit() {
        Boolean addCrowsResult = CrowCalc.addCrows( Limits.getLimitDmlRows() + 1);
        System.assert(addCrowsResult, 'Success addition not "true"');
    }



    // Задание 2: использование helper методов
    // Создайте хелпер метод в тестовом классе или в отдельном классе,
    // который будетупрощать создание тесовых данных.

    public static void createSomeCrows(Integer howMany) {
        List<Crow__c> newCrows = new List<Crow__c>();

        for (Integer i = 0; i < howMany; i++) {
            newCrows.add(new Crow__c());
        }

        insert newCrows;
    }


}