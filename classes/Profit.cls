// Section_5_5

public interface Profit {
    Decimal getProfit();
}