// Section_6_4 - Section practice

public with sharing class ContactSection64TriggerHandler {

    public static void createContact(List<Contact> newContacts) {

        Set<Id> newContactsIds = new Set<Id>();
        List<Contact> contactsFromDb = new List<Contact>();
        List<Contact> contacts = new List<Contact>();
        Set<Id> assignedAccountIds = new Set<Id>();
        Set<Id> freeAccountIds = new Set<Id>();
        List<Account> accountsFromDb = new List<Account>();
        List<Account> accounts = new List<Account>();

        // Fill set by unique Ids of new contacts
        for (Contact cont : newContacts) {
            newContactsIds.add(cont.Id);
        }
        System.debug('newContacts.size() = ' + newContacts.size());
        System.debug('newContactsIds.size() = ' + newContactsIds.size());

        // Get all new contacts and contacts with a full account
        contactsFromDb = [SELECT Id, FirstName, LastName, AccountId, isAccountSet__c FROM Contact WHERE Id IN :newContacts OR AccountId != null];
        System.debug('contactsFromDb.size() = ' + contactsFromDb.size());

        // Divide contacts into new contacts and contacts with a full account
        for (Contact cont : contactsFromDb) {
            if (cont.AccountId != null) {
                assignedAccountIds.add(cont.AccountId);
            }
            if (newContactsIds.contains(cont.Id)) {
                contacts.add(cont);
            }
        }

        // Get free accounts with filled activity
        accountsFromDb = [SELECT Id, Name, Activity__c FROM Account WHERE Activity__c != null AND Id NOT IN :assignedAccountIds];
        System.debug('accountsFromDb.size() = ' + accountsFromDb.size());

        System.debug('accountsFromDb before sort :::: ' + accountsFromDb);
        accounts = ContactSection64TriggerHandler.getAccountSortListByDate(accountsFromDb);
        System.debug('accounts after sort :::::  :::: ' + accounts);

        // Assign free accounts to new contacts
        for (Integer index = 0; index < contacts.size(); index++) {
            if (index < accounts.size()) {
                contacts.get(index).AccountId = accounts.get(index).Id;
                contacts.get(index).isAccountSet__c = true;
                System.debug(contacts.get(index).FirstName + ' ' + contacts.get(index).LastName + ' have account ' + accounts.get(index).Name + ' with ' + accounts.get(index).Activity__c );
            } else {
                contacts.get(index).isAccountSet__c = false;
            }
        }

        update contacts;
    }

    // Sort accounts by Activity date close to average date of all free accounts
    public static List<Account> getAccountSortListByDate(List<Account> accounts) {

        System.debug('getAccountSortListByDate ::: accounts.size() = ' + accounts.size());

        List<Long> deltaTime = new List<Long>();
        Long sumOfDeltaUnix = 0;
        Long averageTimeUnix;
        Long todayDateUnix = ((Datetime)Date.today()).getTime();

        // Solve sum of deltas
        for (Account acc : accounts) {
            sumOfDeltaUnix += ((Datetime)acc.Activity__c).getTime() - todayDateUnix;
        }

        // Calculate average date
        averageTimeUnix = sumOfDeltaUnix / accounts.size() + todayDateUnix;
        System.debug('averageTimeUnix = ' + averageTimeUnix);
        System.debug('averageTime = ' + Datetime.newInstance(averageTimeUnix));

        // Sort list of accounts
        for (Integer indexOuter = 0; indexOuter < accounts.size() - 1; indexOuter++) {
            for (Integer indexInner = indexOuter + 1; indexInner < accounts.size(); indexInner++) {
                if (Math.abs(((Datetime)accounts.get(indexOuter).Activity__c).getTime() - averageTimeUnix) >
                        Math.abs(((Datetime)accounts.get(indexInner).Activity__c).getTime() - averageTimeUnix)) {
                    Account tempAccount = accounts[indexOuter];
                    accounts[indexOuter] = accounts[indexInner];
                    accounts[indexInner] = tempAccount;
                }
            }
        }

        return accounts;
    }


    /*
    // For Anonymous Apex
    List<Contact> conts = new List<Contact>();
    Contact c1 = new Contact(FirstName = 'Section6', LastName = '#14');
    conts.add(c1);
    insert conts;

    // For SOQL Query
    SELECT Id, FirstName, LastName, AccountId, isAccountSet__c FROM Contact WHERE FirstName = 'Section6'

    SELECT Id, Name, Activity__c FROM Account WHERE Name like 'Section6%'
    * */
}