// Section_5 - final

public class AccountSwitcherImpl implements AccountSwitcher {

    public static List<Type> allContacts = new List<Type>();

    /*
    * Метод getAndSort получает все контакты на орге
    * и если поле Amount у контактабольше 10000 то создать объект класса Private Contact Class
    * и заполнить у него поляContactRecord - текущий контакт и CardNumber - рандомное 6-значное число
    * (Если вэтом числе есть хотя бы 3 одинаковые цифры, то в значение поля ContactType будетравно ‘Premier’, иначе ContactType =’Person’).
    * Иначе для текущего контакта создатьобъект Public Contact Class
    * и заполнить у него поля ContactRecord - текущий контакт иVolunteerNumber - рандомное четное 6-значное число.
    * Для всех объектов класса PublicContact, значение поля ContactType =’Person’.
    * Все созданные объекты метод ложит встатическое поле AllContacts.
    * */
    public static void getAndSort() {
        // Receive contacts from org
        List<Contact> contacts = [SELECT Id, Name, FirstName, LastName, Amount__c, AccountId FROM Contact LIMIT 1000];

        if (!contacts.isEmpty()) {
            // Sort contacts and create private contacts and public contacts
            for (Contact cont : contacts) {
                if (cont.Amount__c > 10000) {
                    PrivateContact privateCont = new PrivateContact();

                    privateCont.setContactRecord(cont);
                    privateCont.setCardNumber(getRandomNumber());

                    if (containThreeSameDigits(privateCont.getCardNumber())) {
                        privateCont.setContactType('Premier');
                    } else {
                        privateCont.setContactType('Person');
                    }

                    allContacts.add(privateCont);
                } else {
                    PublicContact publicCont = new PublicContact();

                    publicCont.setContactRecord(cont);
                    publicCont.setVolunteerNumber(getRandomEvenNumber());
                    publicCont.setContactType('Person');

                    allContacts.add(publicCont);
                }
            }
        }
    }

    /*
     * Generates random number between 100000 and 999999
     *
     * @return Integer returns random number
     */
    @TestVisible
    private static Integer getRandomNumber() {
        return Integer.valueOf(Math.random() * 899999) + 100000;
    }

    /*
     * Generates random even number between 100000 and 999998
     *
     * @return Integer returns random even number
     */
    @TestVisible
    private static Integer getRandomEvenNumber() {
        return (Integer.valueOf(Math.random() * 449999) + 50000) * 2;
    }

    /*
     * Сhecks if number contains three identical digits
     *
     * @param numb checked number
     * @return Boolean returns 'true' if number contains three identical digits
     */
    @TestVisible
    private static Boolean containThreeSameDigits(Integer numb) {

        Integer[] countOfDigits = new Integer[] {0,0,0,0,0,0,0,0,0,0};
        Boolean isContain = false;

        while (numb >= 1) {
            Integer digit = Math.mod(numb, 10);
            countOfDigits[digit]++;
            numb = numb / 10;
        }

        for (Integer item : countOfDigits) {
            if (item >= 3) {
                isContain = true;
            }
        }

        return isContain;
    }

    /*
     * Метод switchAccount перебирает лист AllContacts.
     * Он пробегает по полю AllContacts иформирует два листа(List<Private Contact Class> и List<Public Contact Class>) с соответствующими объектами.
     * После этого он меняет местами Account через поле ContactRecord у соответствующих объектов из листов.
     * Например первый элементлиста List<Private Contact Class> меняется Аккаунтом с первым значением листаList<Public Contact Class> и тд.
     * Если у элемента листа List<Private Contact Class>значение поля ContactType = ‘Premier’,
     * то у этого объекта нельзя менять Аккаунт и онпропускается.
     * Далее все Contacts у которых был изменен Account обновляются(Update).
     *
     * Метод возвращает объект внутреннего(inner) класса DataResultс 3-мя заполненными листами -
     * ChangedPrivateContacts - лист со всеми измененнымиPrivate Contacts,
     * ChangedPublicContacts - лист со всеми измененными PublicContacts
     * и NonChangedPrivateContacts - лист со всеми не измененными PrivateContacts (те укоторых ContactType = ‘Premier’)
     *
     * */
    public static DataResult switchAccount() {
        List<Type> privateContacts = new List<Type>();
        List<Type> publicContacts = new List<Type>();
        DataResult dataResults = new DataResult();

        for (Type contItem : allContacts) {
            if (contItem instanceof PrivateContact) {
                privateContacts.add(contItem);
            } else if (contItem instanceof PublicContact) {
                publicContacts.add(contItem);
            }
        }

        List<Type> privatePersonContacts = new List<Type>();
        for (Type contItem : privateContacts) {
            if (contItem.getContactType() == 'Premier') {
                dataResults.NonChangedPrivateContacts.add((PrivateContact)contItem);
            } else if (contItem.getContactType() == 'Person') {
                privatePersonContacts.add((PrivateContact)contItem);
            }
        }

        Integer minSize = Math.min(privatePersonContacts.size(), publicContacts.size());
        List<Contact> contactsToUpdate = new List<Contact>();

        for (Integer index = 0; index < minSize; index++) {
            PrivateContact privCont = (PrivateContact)privatePersonContacts.get(index);
            PublicContact pubCont = (PublicContact)publicContacts.get(index);

            Contact aPrivateContact = ((PrivateContact)privatePersonContacts.get(index)).getContactRecord();
            Contact aPublicContact = ((PublicContact)publicContacts.get(index)).getContactRecord();

            swapAccountIdsOfContacts(aPrivateContact, aPublicContact);

            contactsToUpdate.add(aPrivateContact);
            contactsToUpdate.add(aPublicContact);
        }

        try {
            update contactsToUpdate;
        } catch (Exception ex) {
            System.debug('Error message: ' + ex.getMessage() + '\nLine Number: ' + ex.getLineNumber());
        }

        for (Type contItem : privatePersonContacts) {
            dataResults.ChangedPrivateContacts.add((PrivateContact)contItem);
        }

        for (Type contItem : publicContacts) {
            dataResults.ChangedPublicContacts.add((PublicContact)contItem);
        }

        return dataResults;
    }

    /*
     * Swap accountIds between contacts
     *
     * @param firstContact Contact first contact instance
     * @param secondContact Contact second contact instance
     * @return Boolean returns 'true' if swap was successful
     * */
    @TestVisible
    private static void swapAccountIdsOfContacts(Contact firstContact, Contact secondContact) {
            Id contId = firstContact.AccountId;
            firstContact.AccountId = secondContact.AccountId;
            secondContact.AccountId = contId;
    }

}
