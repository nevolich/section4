// Section_5 - final

@IsTest
private class PrivateContactTest {

    @IsTest
    public static void getAndSetContactTypeTest() {
        PrivateContact privContact = new PrivateContact();
        privContact.setContactType('PrivateContact');
        System.assertEquals('PrivateContact', privContact.getContactType(), 'Incorrect contact type');
    }

    @IsTest
    public static void getAndSetCardNumberTest() {
        PrivateContact privContact = new PrivateContact();
        privContact.setCardNumber(123456);
        System.assertEquals(123456, privContact.getCardNumber(), 'Incorrect card number');
    }

    @IsTest
    public static void getAndSetContactRecordTest() {
        PrivateContact privContact = new PrivateContact();
        Contact aContact = new Contact(LastName = 'LastNameTest');
        privContact.setContactRecord(aContact);
        System.assertEquals(aContact, privContact.getContactRecord(), 'Incorrect contact record');
        System.assertEquals('LastNameTest', privContact.getContactRecord().LastName, 'Incorrect name of contact record');
    }

}