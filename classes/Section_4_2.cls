//// SOQL
public with sharing class Section_4_2 {

/*

//Написать SOQL запрос для получения таблицы из объекта Contact c полями Id, Name, атакже поле Name связанного с ним Аккаунта где LastName ‘Bond’

    SELECT Id, Name, Account.Name FROM Contact WHERE LastName = 'Bond'



//Написать SOQL запрос для получения таблицы из объекта Contact c полями Id, Name, атакже поле Name связанного с ним Аккаунта где LastName будет начинаться с D’

    SELECT Id, Name, Account.Name FROM Contact WHERE LastName like 'D%'



//Написать SOQL запрос для получения таблицы из объекта Contact c полями Id, Name, атакже поле Name связанного с ним Аккаунта и отсортировать поLastName(возрастание)

    SELECT Id, Name, Account.Name FROM Contact ORDER BY LastName ASC



//Написать SOQL запрос для получения таблицы из объекта Contact c полями Id, Name, атакже поле Name связанного с ним Аккаунта с 4 по 15 записи

    SELECT Id, Name, Account.Name FROM Contact LIMIT 12 OFFSET 3



//Написать SOQL запрос для получения таблицы из объекта Contact c полями Id, Name, атакже поле Name связанного с ним Аккаунта где FirstName Tim или LastName Forbes

    SELECT Id, Name, Account.Name FROM Contact WHERE FirstName = 'Tim' AND LastName = 'Forbes'



//Написать SOQL запрос для получения таблицы из объекта Opportunity смаксимальными Amount

    SELECT MAX(Amount) FROM Opportunity



//Написать SOQL запрос для получения таблицы из объекта Opportunity с Amount более 500.000

    SELECT Id, Name, Amount  FROM Opportunity WHERE Amount > 500000



//Написать SOQL запрос для получения таблицы из объекта Lead у которой есть сотрудники

    SELECT Id,  LastName, FirstName, NumberOfEmployees  FROM Lead WHERE NumberOfEmployees > 0

*/

}