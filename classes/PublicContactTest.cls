// Section_5 - final

@IsTest
private class PublicContactTest {

    @IsTest
    public static void getAndSetContactTypeTest() {
        PublicContact pubContact = new PublicContact();
        pubContact.setContactType('PublicContact');
        System.assertEquals('PublicContact', pubContact.getContactType(), 'Incorrect contact type');
    }

    @IsTest
    public static void getAndSetCardNumberTest() {
        PublicContact pubContact = new PublicContact();
        pubContact.setVolunteerNumber(123456);
        System.assertEquals(123456, pubContact.getVolunteerNumber(), 'Incorrect volunteer number');
    }

    @IsTest
    public static void getAndSetContactRecordTest() {
        PublicContact pubContact = new PublicContact();
        Contact aContact = new Contact(LastName = 'LastNameTest');
        pubContact.setContactRecord(aContact);
        System.assertEquals(aContact, pubContact.getContactRecord(), 'Incorrect contact record');
        System.assertEquals('LastNameTest', pubContact.getContactRecord().LastName, 'Incorrect name of contact record');
    }

}