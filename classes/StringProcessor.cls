/**
 * Created by locadmin on 12.09.2019.
 */

public interface StringProcessor {


    String addPrefix (String str);
    String addPostfix (String str);
    String removeWhitespaces (String str);

}