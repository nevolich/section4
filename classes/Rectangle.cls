/**
 * Created by locadmin on 11.09.2019.
 */

public with sharing class Rectangle {

    private Integer length;
    private Integer width;
    private String colour;
    private Datetime createTime;

    public Rectangle() {
        this.length = 0;
        this.width = 0;
        this.colour = '';
        this.createTime = System.now();
    }


    public Rectangle(Integer length, Integer width) {
        this.length = length;
        this.width = width;
        this.createTime = System.now();
    }

    public Rectangle(Integer length, Integer width, String colour) {
        this.length = length;
        this.width = width;
        this.colour = colour;
        this.createTime = System.now();
    }

    public void SetLength(Integer length) {
        this.length = length;
    }

    public void SetWidth(Integer width) {
        this.width = width;
    }

    public void SetColour(String colour) {
        this.colour = colour;
    }

    public String GetPerimeter() {

        if ( 0 == length || 0 == width ) {
            return 'No values assigned to sides.';
        }
        else {
            return (( this.length + this.width ) * 2).format();
        }
    }

    public String GetSquare() {
        if ( 0 == length || 0 == width ) {
            return 'No values assigned to sides.';
        }
        else {
            return (this.length * this.width).format();
        }
    }

    public String GetDiagonal() {
        if ( 0 == length || 0 == width ) {
            return 'No values assigned to sides.';
        }
        else {
            return (Math.sqrt( Math.pow(this.length, 2) + Math.pow(this.width, 2))).format();
        }
    }

    public String GetColour() {
        if ( String.ISBLANK(colour) ) {
            return 'No colour assigned to rectangle.';
        }
        else {
            return colour;
        }
    }

    public String GetCreateTime() {
        return createTime.format();
    }

}