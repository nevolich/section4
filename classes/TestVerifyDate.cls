// Section_5_2 - Apex Tests
// Trailhead "Apex Testing"

@IsTest
private class TestVerifyDate {

    @isTest
    static void testCheckDates() {

        Date wrongOrderDatesResult = VerifyDate.CheckDates(Date.newInstance(2019, 09, 23), Date.newInstance(2019, 09, 02));
        Date equalDatesResult     = VerifyDate.CheckDates(Date.newInstance(2019, 09, 23), Date.newInstance(2019, 09, 23));
        Date dateNext29DaysResult = VerifyDate.CheckDates(Date.newInstance(2019, 09, 23), Date.newInstance(2019, 10, 22));
        Date dateNext30DaysResult = VerifyDate.CheckDates(Date.newInstance(2019, 09, 23), Date.newInstance(2019, 10, 23));
        Date dateNext31DaysResult = VerifyDate.CheckDates(Date.newInstance(2019, 09, 23), Date.newInstance(2019, 10, 24));

        System.assertEquals(Date.newInstance(2019, 09, 30), wrongOrderDatesResult, 'Wrong: wrong order dates. Must be end of the month');
        System.assertEquals(Date.newInstance(2019, 09, 23), equalDatesResult, 'Wrong: Equal dates. Must be date2');
        System.assertEquals(Date.newInstance(2019, 10, 22), dateNext29DaysResult, 'Wrong: date2 is within the next 29 days of date1. Must be date2');
        System.assertEquals(Date.newInstance(2019, 09, 30), dateNext30DaysResult, 'Wrong: date2 is within the next 30 days of date1. Must be end of the month');
        System.assertEquals(Date.newInstance(2019, 09, 30), dateNext31DaysResult, 'Wrong: date2 is within the next 31 days of date1. Must be end of the month');

    }
/*
    @isTest
    static void testDateWithin30Days() {

        Date startDate = Date.newInstance(2019, 09, 23);
        Date dateBeforeStartDate = Date.newInstance(2019, 09, 02);
        Date dateNext29Days = Date.newInstance(2019, 10, 22);
        Date dateNext30Days = Date.newInstance(2019, 10, 23);
        Date dateNext31Days = Date.newInstance(2019, 10, 24);

        Boolean beforeStartDateResult = VerifyDate.DateWithin30Days(startDate, dateBeforeStartDate);
        Boolean dateNext29DaysResult = VerifyDate.DateWithin30Days(startDate, dateNext29Days);
        Boolean dateNext30DaysResult = VerifyDate.DateWithin30Days(startDate, dateNext30Days);
        Boolean dateNext31DaysResult = VerifyDate.DateWithin30Days(startDate, dateNext31Days);

        System.assertEquals(false, beforeStartDateResult, 'Wrong order is not \'false\'');
        System.assertEquals(false, dateNext29DaysResult, 'Date2 less then 29 days is not \'false\'');
        System.assertEquals(false, dateNext30DaysResult, 'Date2 less then 30 days is not \'false\'');
        System.assertEquals(true,  dateNext31DaysResult, 'Date2 less then 31 days is not \'true\'');

    }
*/

}