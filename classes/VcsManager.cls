// Section_5_5

public with sharing class VcsManager {

    private static final Map<String, VersionControlSystem> strategies = new Map<String, VersionControlSystem> {
            'Git' => (VersionControlSystem) Type.forName('GitLabVcs').newInstance(),
            'BitBucket' => (VersionControlSystem) Type.forName('BitBucketVcs').newInstance()
    };

    private VersionControlSystem strategy;

    public VcsManager(String strategyName) {
        strategy = strategies.get(strategyName);
    }

    public String makePush() {
        return strategy.makePush();
    }

    public String makePull() {
        return strategy.makePull();
    }

    public String makeCommit() {
        return strategy.makeCommit();
    }
}


/*

    /*
        VcsManager vcs = new VcsManager('Git');
        System.debug(vcs.makePull());
        System.debug(vcs.makePush());
        System.debug(vcs.makeCommit());

        vcs = new VcsManager('BitBucket');
        System.debug(vcs.makePull());
        System.debug(vcs.makePush());
        System.debug(vcs.makeCommit());
    * */

* */