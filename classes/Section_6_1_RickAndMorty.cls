// Section_6_1 - Triggers

public with sharing class Section_6_1_RickAndMorty {

    public static void task1CreateRick(Integer howMany) {
        List<Contact> ricks = new List<Contact>();
        for (Integer index = 0; index < howMany; index++) {
            Contact newRick = new Contact(FirstName = 'Rick', LastName = 'Sanchez', Title = 'task1 #' + index);
            ricks.add(newRick);
        }
        insert ricks;
    }

    public static List<Contact> task2CreateRick(Integer howMany) {
        List<Contact> ricks = new List<Contact>();
        for (Integer index = 0; index < howMany; index++) {
            Contact newRick = new Contact(FirstName = 'Rick', LastName = 'Sanchez', Title = 'task2 #' + index);
            ricks.add(newRick);
        }
        insert ricks;
        return ricks;
    }

    public static List<Contact> task2CreateMorty(Integer howMany) {
        List<Contact> morties = new List<Contact>();
        for (Integer index = 0; index < howMany; index++) {
            Contact newMorty = new Contact(FirstName = 'Morty', LastName = 'Smith', Title = 'task2 #' + index);
            morties.add(newMorty);
        }
        insert morties;
        return morties;
    }

    public static void task2UpdateMorty() {
        List<Contact> ricks = task2CreateRick(1);
        List<Contact> morties = task2CreateMorty(2);

        morties.get(0).MyRick__c = null;
        morties.get(0).MyRick__c = ricks.get(0).Id;

        update morties;
    }

    public static void task3CreateRickOrMorty(Integer howMany) {
        List<Contact> persons = new List<Contact>();

        for (Integer index = 0; index < howMany; index++) {
            if ( Integer.valueOf(Math.random() * 2) == 0 ) {
                Contact newRick = new Contact(FirstName = 'Rick', LastName = 'Sanchez', Title = 'task3 #' + index);
                persons.add(newRick);
            } else {
                Contact newMorty = new Contact(FirstName = 'Morty', LastName = 'Smith', Title = 'task3 #' + index);
                persons.add(newMorty);
            }
        }
        insert persons;

        //delete [SELECT Id FROM Contact WHERE Title like 'task3%' LIMIT 100];
    }

    public static void task4CreateRickOrMorty(Integer howMany) {
        task3CreateRickOrMorty(howMany);
    }


/*
    List<Contact> conts = new List<Contact>();
    Contact rick1 = new Contact(FirstName = 'Rick', LastName = 'Sanchez'); conts.add(rick1);
    Contact rick2 = new Contact(FirstName = 'Rick', LastName = 'Sanchez'); conts.add(rick2);
    insert conts;

    List<Contact> conts = new List<Contact>();
    Contact morty1 = new Contact(FirstName = 'Morty', LastName = 'Smith'); conts.add(morty1);
    Contact morty2 = new Contact(FirstName = 'Morty', LastName = 'Smith'); conts.add(morty2);
    insert conts;

    Contact m = [SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c FROM Contact WHERE FirstName = 'Morty' AND LastName = 'Smith' and MyRick__c = null LIMIT 1 ];
    //m.Phone = '12345';
    m.MyRick__c = '0032v00002xhjQ4AAI';
    update m;

*/

/*
    SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title FROM Contact WHERE (FirstName = 'Rick' AND LastName = 'Sanchez') OR (FirstName = 'Morty' AND LastName = 'Smith') ORDER BY FirstName LIMIT 100

    delete [SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c FROM Contact WHERE (FirstName = 'Rick' AND LastName = 'Sanchez') OR (FirstName = 'Morty' AND LastName = 'Smith') LIMIT 100];

    delete [SELECT Id FROM Contact WHERE FirstName = 'Morty' AND LastName = 'Smith' AND MyRick__c = null LIMIT 100];


    // Task 1, 2
    // Add Ricks
    List<Contact> conts = new List<Contact>();
    Contact rick1 = new Contact(FirstName = 'Rick', LastName = 'Sanchez'); conts.add(rick1);
    Contact rick2 = new Contact(FirstName = 'Rick', LastName = 'Sanchez'); conts.add(rick2);
    insert conts;

    // Add Morty
    List<Contact> conts = new List<Contact>();
    Contact morty1 = new Contact(FirstName = 'Morty', LastName = 'Smith');
    conts.add(morty1);
    insert conts;

    // Update Morty
    Contact m = [SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c FROM Contact WHERE FirstName = 'Morty' AND LastName = 'Smith' and MyRick__c = null LIMIT 1 ];
    //m.Phone = '12345';
    m.MyRick__c = '0032v00002xhjQ4AAI';
    update m;

*/
}