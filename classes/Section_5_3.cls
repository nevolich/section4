/**
Salesforce Limits
 */



public with sharing class Section_5_3 {


/*
Salesforce LimitsTask
1. Что не так с этим кодом? Какой лимит может быть превышен и как можно этоисправить? Исправьте этот код.
*/
    public static void task_1() {

        List<Contact> firstContacts = new List<Contact>();
        for (Integer i = 0; i < 200; i++) {
            firstContacts.add([SELECT Name FROM Contact LIMIT 1 OFFSET :i]);
        }

        // System.LimitException: Too many SOQL queries: 101
        // Превышение SOQL запросов: 100 (синхр), 200 (асинхр)

        // исправленная
        List<Contact> contacts = [SELECT Name FROM Contact LIMIT 200];
        List<Contact> firstContactsFix = new List<Contact>();
        for (Contact contact : contacts) {
            firstContactsFix.add(contact);
        }

    }
    /*
    Task 2
    Что не так с этим кодом? Какой лимит может быть превышен и как можно этоисправить? Исправьте этот код.
    */
    public static void task_2() {
/*
        for (Integer i = 0; i < 200; i++) {
            Contact cnt = new Contact(lastName = 'test', firstName = 'test' + i);
            Insert cnt;
        }

        //System.LimitException: Too many DML statements: 151
        // Вынести DML операцию за пределы цикла
*/
        // исправленная
        System.debug(Database.countQuery('SELECT COUNT() FROM Contact WHERE lastName = \'test\'') );
        Savepoint sp1 = Database.setSavepoint();

        List<Contact> contacts = new List<Contact>();
        for (Integer i = 0; i < 200; i++) {
            Contact cnt = new Contact(lastName = 'test', firstName = 'test' + i);
            contacts.add(cnt);
        }
        Insert contacts;

        Contact cnt = new Contact(lastName = 'test', firstName = 'test' );
        insert cnt;

        System.debug(Database.countQuery('SELECT COUNT() FROM Contact WHERE lastName = \'test\'') );
        Database.rollback(sp1);
    }

    /*
    Task 3
    Что не так с этим кодом? Какой лимит может быть превышен и как это исправить?
    Исправьте этот код.
    */
    public static void task_3() {

        Set<String> allNames = new Set<String>();
        List<Contact> allContacts = [SELECT firstName FROM Contact];

        for (Contact contactItem : allContacts) {
            allNames.add(contactItem.firstName);
        }

        //Success.
        //Получение большого количества результирующих строк через SOQL - 50000
        // или System.LimitException: Apex CPU time limit exceeded - более 10 секунд если большой цикл
    }

    /*
    Task 4
    Ниже даны два апекс класса: триггер и хэндлер. Что не так с этим кодом (в планериска превысить лимиты)?
    Какой лимит может быть превышен и как это исправить? Исправьте этот код.
    */
    public static void task_4() {
    /*
        //Trigger:
        trigger agentSmithTrigger on Contact (before update) {
            for(Contact target: trigger.new) {
                agentSmithTriggerHandler.infect(target);
            }
        }

        //Handler:
        public with sharing class agentSmithTriggerHandler {
            public static void infect(Contact target) {
                Contact agentSmith = [SELECT Id FROM Contact WHERE ];
                target.infectedBy = agentSmith.id;
            }
        }

        // Сумма вызовов SOQL
        // Total stack depth for any Apex invocation that recursively fires triggers due to insert, update, or delete statements	- 16
        // Maximum number of characters for a trigger	- 1 million


    */
    }

// Links:https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_gov_limits.htm



}