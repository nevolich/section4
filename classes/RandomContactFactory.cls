// Section_5_2 - Apex Tests
// Trailhead "Apex Testing"

public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer numContacts, String newLastName) {
        List<Contact> contacts = new List<Contact>();

        for(Integer i = 0; i < numContacts; i++) {
            Contact c = new Contact(FirstName = 'Test ' + i, LastName = newLastName);
            contacts.add(c);
        }

        return contacts;
    }
}