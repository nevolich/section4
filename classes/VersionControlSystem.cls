// Section_5_5
/*
* Interface for Version Control System
*
* */
public interface VersionControlSystem {
    String makePush();
    String makePull();
    String makeCommit();
}

