/**
 * Created by locadmin on 19.09.2019.
 */

public class ExampleRandom {

    public static void Random () {
        //Section_4_5.task_2();


        //Get random number between 0 and 10 Apex
        Integer randomNumber = Integer.valueof((Math.random() * 10));
        System.debug('randomNumber  is ' + randomNumber);

        //Get random number between 0 and 100 Apex
        Integer randomNumber100 = Integer.valueof((Math.random() * 100));
        System.debug('randomNumber100 is ' + randomNumber100);

        //Get random Boolean value Apex
        Integer randomNumberBool = Integer.valueof((math.random() * 10));
        Boolean randomBoolean = Math.mod(randomNumberBool, 2) == 0 ? true : false;
        System.debug('randomBoolean is ' + randomBoolean);

        //Get random String from list of strings Apex
        List<String> availableValues = new List<String>{
                'Red', 'Green', 'Blue', 'White', 'Black'
        };
        Integer listSize = availableValues.size() - 1;
        Integer randomNumberSt = Integer.valueof((Math.random() * listSize));
        String randomString = availableValues[randomNumberSt];
        System.debug('randomString is ' + randomString);

    }



}