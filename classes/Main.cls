/**
 * Created by locadmin on 11.09.2019.
 */

public class Main {

    public static  void Start() {

        Rectangle rect = new Rectangle(0, 0);
        System.debug('Perimeter: ' + rect.GetPerimeter());
        System.debug('Square: ' + rect.GetSquare());
        System.debug('Colour: ' + rect.GetColour());

        rect.SetLength(10);
        rect.SetWidth(5);
        rect.SetColour('Black');

        System.debug('Perimeter: ' + rect.GetPerimeter());
        System.debug('Square: ' + rect.GetSquare());
        System.debug('Diagonal: ' + rect.GetDiagonal());
        System.debug('Colour: ' + rect.GetColour());
        System.debug('Create time: ' + rect.GetCreateTime());


      /*  for (Integer i = 0; i < 9; i++) {
            rect.SetLength(i);
            System.debug('Perimeter: ' + rect.GetPerimeter());
        }
        */


        /*
        For Anonymous Apex:

        Main.start();
        */
        /*
        Rectangle rect = new Rectangle();

        for (Integer i = 0; i < 9; i++) {
            rect.SetLength(i);
            rect.SetWidth(i);
            System.debug('Perimeter: ' + rect.GetPerimeter());
            System.debug('Square: ' + rect.GetSquare());
        }
        */

    }

}