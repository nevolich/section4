// Section_5 - final

public virtual class Type {

    protected String contactType;
    protected Decimal amountOfMoney;

    public virtual String getContactType() {
        return contactType;
    }

    public void setContactType(String value) {
        contactType = value;
    }

    public Decimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Decimal value) {
        amountOfMoney = value;
    }

}