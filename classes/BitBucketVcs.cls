// Section_5_5

public with sharing class BitBucketVcs implements VersionControlSystem {
    public String makePush() {
        return('Pushed to BitBucket');
    }

    public String makePull() {
        return('Pulled to BitBucket');
    }

    public String makeCommit() {
        return('Committed to BitBucket');
    }

}
