/**
 * Created by locadmin on 12.09.2019.
 */

public class MyStringProcessor implements StringProcessor {

    private String prefix = '';
    private String postfix = '';

    public void SetPrefix(String str) {
        prefix = str;
    }

    public String GetPrefix() {
        return prefix;
    }

    public void SetPostfix(String str) {
        postfix = str;
    }

    public String GetPostfix() {
        return postfix;
    }

    public String addPrefix (String str) {
        //System.debug('addPrefix');
        return prefix + str;
    }

    public String addPostfix (String str) {
        //System.debug('addPostfix');
        return str + postfix;
    }

    public String removeWhitespaces (String str) {
        //System.debug('removeWhitespaces');
        return str.replace(' ', '');
    }

    public void updateList (List<String> inputList, List<String> outputList) {
        for (String s : inputList) {
            outputList.add( removeWhitespaces(s) );
        }
    }

}



//For Anonymous Apex:

/*
MyStringProcessor msp = new MyStringProcessor();
String str = 'Hi Jack';
msp.setPrefix('#');
msp.setPostfix('!');

System.debug('\'' + str + '\' add prefix => \'' + msp.addPrefix(str) + '\'');

System.debug('\'' + str + '\' add postfix => \'' + msp.addPostfix(str) + '\'');

System.debug('\'' + str + '\' remove whitespaces => \'' + msp.removeWhitespaces(str) + '\'');

*/


/*
List<String> inputList = new List<String>();
List<String> outputList = new List<String>();

inputList.add('on Monday');
inputList.add('in September');
inputList.add('in the morning');
inputList.add('from morning till night');
inputList.add('late autumn');
inputList.add('early morning');
inputList.add('a light lunch');
inputList.add('We have breakfast at 9 o’clock');
inputList.add('in the winter of 1950');
inputList.add('I know French well');

for (String s : inputList) {
    outputList.add(msp.addPrefix(s));
}

System.debug('');
System.debug('inputList (' + inputList.size()+ ' elements)' );
for (String s : inputList) {
    System.debug(s);
}

System.debug('');
System.debug('outputList (' + outputList.size()+ ' elements)' );
for (String s : outputList) {
    System.debug(s);
}

outputList.clear();
msp.updateList(inputList, outputList);

System.debug('');
System.debug('outputList (' + outputList.size()+ ' elements)' );
for (String s : outputList) {
    System.debug(s);
}
*/