// Section_5_5

public with sharing class GitLabVcs implements VersionControlSystem {
    public String makePush() {
        return('Pushed to Git');
    }

    public String makePull() {
        return('Pulled to Git');
    }

    public String makeCommit() {
        return('Committed to Git');
    }
}