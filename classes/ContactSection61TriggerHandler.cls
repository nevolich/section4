// Section_6_1

public with sharing class ContactSection61TriggerHandler {

    // Task 1
    public static void handleAfterInsert611(List<Contact> newContacts) {
        List<Contact> morties = new List<Contact>();

        for (Contact contItem : newContacts) {
            if (contItem.FirstName == 'Rick' && contItem.LastName == 'Sanchez') {
                Contact morty = new Contact(FirstName = 'Morty', LastName = 'Smith');
                morties.add(morty);
            }
        }

        insert morties;
    }

    // Task 2.1 - After insert for Rick
    public static void handleAfterInsert612(List<Contact> newContacts) {
        System.debug(':::: handleAfterInsert612 :::: Start');
        List<Contact> morties = new List<Contact>();
        List<Contact> contacts = [SELECT Id, FirstName, LastName FROM Contact WHERE id = :newContacts];

        for (Contact contItem : contacts) {
            if (contItem.FirstName == 'Rick' && contItem.LastName == 'Sanchez') {
                Contact morty = new Contact(FirstName = 'Morty', LastName = 'Smith', MyRick__c = contItem.Id);
                morties.add(morty);
            }
        }

        insert morties;
    }

    // Task 2.2 - Before insert and Updete for Morty
    public static void handleBeforeInsertUpdate612(List<Contact> newContacts) {
        System.debug(':::: handleBeforeInsertUpdate612 :::: Start');
        //List<Contact> morties = new List<Contact>();

        for (Contact contItem : newContacts) {
            if (contItem.FirstName == 'Morty' && contItem.LastName == 'Smith') {
                contItem.SadMorty__c = contItem.MyRick__c == null ? true : false;
                //morties.add(contItem);
            }
        }
    }

    // Task 3 - Assign Morty to Rick from the same list
    public static void handleAfterInsert613(List<Contact> newContacts) {
        System.debug(':::: handleAfterInsert613 :::: Start');

        List<Contact> ricks = new List<Contact>();
        List<Contact> morties = new List<Contact>();

        List<Contact> contacts = [SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title FROM Contact WHERE Id IN :newContacts];

        for (Contact contItem : contacts) {
            if (contItem.FirstName == 'Rick' && contItem.LastName == 'Sanchez') {
                ricks.add(contItem);
            } else if (contItem.FirstName == 'Morty' && contItem.LastName == 'Smith') {
                morties.add(contItem);
            }
        }

        Integer minSize = Math.min(ricks.size(), morties.size());
        System.debug('minSize = ' + minSize);

        for (Integer index = 0; index < minSize; index++) {
            morties.get(index).MyRick__c = ricks.get(index).Id;
            //System.debug('morty : ' + morties.get(index).Title + ' assign ' + ricks.get(index).Title);
        }

        update morties;
    }

    // Task 4 - Assign Morty to Rick from the same list or database
    public static void handleAfterInsert614(List<Contact> newContacts) {
        System.debug(':::: handleAfterInsert614 :::: Start');

        List<Contact> contactsFromDb = new List<Contact>();
        Set<Id> newContactIds = new Set<Id>();
        List<Contact> morties = new List<Contact>();
        Set<Id> assignedRickIds = new Set<Id>();
        List<Contact> ricks = new List<Contact>();

        // Fill set with Ids of new contacts
        for (Contact cont : newContacts) {
            newContactIds.add(cont.Id);
        }

        // Get Rick and Morty from DB
        contactsFromDb = [
                SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title
                FROM Contact
                WHERE (FirstName = 'Rick' AND LastName = 'Sanchez')
                    OR (FirstName = 'Morty' AND LastName = 'Smith')
        ];

        // Search Morties to fill list of assigned Rick and to fill list of new Morties
        for (Contact cont : contactsFromDb) {
            if (cont.FirstName == 'Morty' && cont.LastName == 'Smith') {
                if (cont.MyRick__c != null) {
                    assignedRickIds.add(cont.MyRick__c);
                }
                if (newContactIds.contains(cont.Id)) {
                    morties.add(cont);
                }
            }
        }

        // Fill list with free Ricks
        for (Contact cont : contactsFromDb) {
            if (cont.FirstName == 'Rick' && cont.LastName == 'Sanchez' && !assignedRickIds.contains(cont.Id)) {
                ricks.add(cont);
            }
        }

        // Assign free Ricks to new Morties
        Integer minSize = Math.min(ricks.size(), morties.size());

        for (Integer index = 0; index < minSize; index++) {
            morties.get(index).MyRick__c = ricks.get(index).Id;
        }

        update morties;
    }

    // Task 4 - Assign Morty to Rick from the same list or database - Alternative with many SOQL
    public static void handleAfterInsert614Alternative(List<Contact> newContacts) {
        System.debug(':::: handleAfterInsert614Alternative :::: Start');

        List<Contact> morties = new List<Contact>();
        List<Contact> mortiesWithRick = new List<Contact>();
        List<Id> assignedRickIds = new List<Id>();
        List<Contact> assignedRicks = new List<Contact>();
        List<Contact> ricks = new List<Contact>();

        morties = [
                SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title
                FROM Contact
                WHERE FirstName = 'Morty' AND LastName = 'Smith' AND Id in :newContacts
        ];
        System.debug('morties.size = ' + morties.size());

        mortiesWithRick = [
                SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title
                FROM Contact
                WHERE FirstName = 'Morty' AND LastName = 'Smith' AND MyRick__c != null
        ];
        System.debug('mortiesWithRick.size = ' + mortiesWithRick.size());

        for (Contact morty : mortiesWithRick) {
            assignedRickIds.add(morty.MyRick__c);
        }
        System.debug('assignedRickIds.size = ' + assignedRickIds.size());

        ricks = [
                SELECT Id, FirstName, LastName, MyRick__c, SadMorty__c, Title
                FROM Contact
                WHERE FirstName = 'Rick' AND LastName = 'Sanchez' AND Id NOT IN :assignedRickIds
        ];
        System.debug('ricks.size = ' + ricks.size());

        // Assign free Ricks to new Morties
        Integer minSize = Math.min(ricks.size(), morties.size());

        for (Integer index = 0; index < minSize; index++) {
            morties.get(index).MyRick__c = ricks.get(index).Id;
            //System.debug('morty : ' + morties.get(index).Title + ' assign ' + ricks.get(index).Title);
        }

        update morties;
    }
}