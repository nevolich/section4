// Section_6_1

trigger ContactTrigger_Section_6_1_0 on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    System.debug('Creating new contact');

    if (Trigger.isBefore && Trigger.isInsert) {
        System.debug('Trigger before insert');
        System.debug(Trigger.new.get(0));
    } else if (Trigger.isAfter && Trigger.isInsert) {
        System.debug('Trigger after insert');
        System.debug('Trigger size ' + Trigger.size);
        System.debug(Trigger.new);
    } else if (Trigger.isAfter && Trigger.isUpdate) {
        System.debug('Trigger after update');
        System.debug('Trigger size ' + Trigger.size);
        System.debug(Trigger.new);
    }
}