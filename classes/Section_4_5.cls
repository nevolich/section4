public class Section_4_5 {

    /*
    1. Создать дата модель. Объект Деталь со связью на Двигатель. ОбъектДвигатель.
    Объект Автомобиль со связью на двигатели так, чтоб одиндвигатель содержал все автомобили в которых используется этот двигатель.

    2. ​Заполнить объекты данными при помощи кода и цикла. Не менее 100 записей.
    3. Получить в Лист все Двигатели.
    4. Получить в Листы все Автомобили для каждого двигателя.
    5. Создать Мапу где ключ это Id автомобиля и значения это все деталиавтомобиля.
    6. Создать Мапу где ключ это название двигателя, а значения это названиявсех Автомобилей.
    7. Склонировать все данные в БД так, чтоб каждая запись повторялась(т.к.будет по два одинаковых двигателя, автомобиля и т.п.)
    8. Удалить все дубликаты с помощью повторной выборки данных из объектов ипутем их перебора. Используйте Мапу для сбора уникальных записей в БД.

    Пункты (3-8 должны быть отдельными методами в одном классе).
    */


    public static void task_1() {

        List<Car__c> cars = new List<Car__c> ();

        cars = [SELECT Id, Name, Brand__c, Year__c FROM Car__c LIMIT 15];
        System.debug(cars.size());

        //Car__c newCar = new Car__c();
        Car__c newCar = new Car__c (Name = 'Audi', Brand__c = '80 B4', Year__c = '1989');
        cars.add(newCar);
        System.debug(cars.size());


        Engine__c newEngine = new Engine__c (Name = 'JTD', Volume__c = 1.6, Fuel_Type__c = 'Petrol');
        insert newEngine;

        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c WHERE Name = 'JTD' LIMIT 15];
        newEngine = existEngines[0];

        cars[cars.size() - 1].Engine__c = newEngine.Id;
        upsert cars;

    }


    // 2. ​Заполнить объекты данными при помощи кода и цикла. Не менее 100 записей.
    public static void task_2() {

        List<Engine__c> engines = new List<Engine__c> ();

        for (Integer index = 0; index < 10; index++) {

            Engine__c newEngine = new Engine__c (
                    Name = 'Engine ' + index,
                    Volume__c = Integer.valueof(Math.random() * 15) / 10.0 + 1,
                    Fuel_Type__c = Math.mod(Integer.valueof(Math.random() * 10), 2) == 0 ? 'Petrol' : 'Diesel'
            );

            System.debug('newEngine #' + index + ': ' + newEngine);

            engines.add(newEngine);
        }

        insert engines;
        System.debug('engines.size = ' + engines.size());


        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c WHERE Name like 'Engine%'];
        Integer existEnginesSize = existEngines.size() - 1;
        System.debug('existEngines.size = ' + existEngines.size());

        List<Car__c> cars = new List<Car__c> ();
        for (Integer index = 0; index < 100; index++) {

            Car__c newCar = new Car__c (
                    Name = 'Car ' + index,
                    Brand__c = 'Das Auto ' + index,
                    Year__c = String.valueOf(Integer.valueof(Math.random() * 30) + 1980)
            );
            System.debug('newCar #' + index + ': ' + newCar);

            Integer randomIndexOfEngine = Integer.valueof((Math.random() * (existEnginesSize - 1)));
            newCar.Engine__c = existEngines[randomIndexOfEngine].Id;
            cars.add(newCar);
        }

        insert cars;
        System.debug('cars.size = ' + cars.size());

        List<Car__c> existCars = [SELECT Id, Name, Brand__c, Year__c FROM Car__c WHERE Name like 'Car%'];
        System.debug('existCars.size = ' + existCars.size());



        //List<Engine__c>
        existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c WHERE Name like 'Engine%'];
        //Integer
        existEnginesSize = existEngines.size();
        System.debug('existEngines.size = ' + existEngines.size());

        List<Detail__c> details = new List<Detail__c> ();
        for (Integer index = 0; index < 100; index++) {

            String newDetailCode = String.valueOf(Integer.valueof(Math.random() * 1000000000));
            if (newDetailCode.length() < 9) {
                newDetailCode = '0'.repeat(9 - newDetailCode.length()) + newDetailCode;
            }

            Detail__c newDetail = new Detail__c (
                    Name = 'Detail ' + index,
                    Code__c = newDetailCode
            );
            System.debug('newDetail #' + index + ': ' + newDetail);

            Integer randomIndexOfEngine = Integer.valueof((Math.random() * (existEnginesSize - 1)));
            newDetail.Engine__c = existEngines[randomIndexOfEngine].Id;
            details.add(newDetail);
        }

        insert details;
        System.debug('details.size = ' + details.size());

        List<Detail__c> existDetails = [SELECT Id, Name, Engine__c FROM Detail__c WHERE Name like 'Detail%'];
        System.debug('existDetails.size = ' + existDetails.size());



    }


    // 3. Получить в Лист все Двигатели.
    public static void task_3() {

        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c ];
        System.debug('existEngines.size = ' + existEngines.size());

    }


    //4. Получить в Листы все Автомобили для каждого двигателя.
    public static void task_4() {

        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c ];
        System.debug('existEngines.size = ' + existEngines.size());


        List<List<Car__c>> cars = new List<List<Car__c>>();

        for (Integer index = 0; index < existEngines.size(); index++){
            cars.add(new List<Car__c>());
        }

        System.debug('cars.size() = ' + cars.size());
        System.debug('cars.get(0).size() = ' + cars.get(0).size());


        for (Integer index = 0; index < existEngines.size(); index++) {
            Id engineId = existEngines[index].Id;
            List<Car__c> searchCars = [SELECT Id, Name, Brand__c, Year__c, Engine__c FROM Car__c WHERE Engine__c = :engineId];

            cars[index].addAll(searchCars);

            System.debug('Cars with engine \'' + existEngines[index].Name + '\': '  + searchCars.size());
        }

        System.debug('ALL Cars are grouped by Engine:\'' + cars);


    }


    // 5. Создать Мапу где ключ это Id автомобиля и значения это все детали автомобиля.
    public static void task_5() {

        List<Car__c> existCars = [SELECT Id, Name, Brand__c, Year__c, Engine__c FROM Car__c];
        List<Detail__c> existDetails = [SELECT Id, Name, Code__c, Engine__c FROM Detail__c];

        Map<Id, List<Detail__c>> carDetailsById = new Map<Id, List<Detail__c>>();


        for (Integer index = 0; index < existCars.size(); index++) {
        //for (Car__c car : existCars)

            ///Error on line 177, column 1: System.LimitException: Too many SOQL queries: 101
            //List<Detail__c> details = [SELECT Id, Name, Code__c, Engine__c FROM Detail__c WHERE Engine__c = :existCars[index].Engine__c];

            List<Detail__c> detailsOfCurrentCar = new List<Detail__c>();

            for (Detail__c checkDetail : existDetails) {
                if (checkDetail.Engine__c == existCars[index].Engine__c) {
                    detailsOfCurrentCar.add(checkDetail);
                }
            }

            carDetailsById.put(existCars[index].Id, detailsOfCurrentCar);

            System.debug('Car \'' + existCars[index].Name + ' ' + existCars[index].Brand__c + '\' include ' + detailsOfCurrentCar.size() + ' details.');
        }

        System.debug('carDetailsById = ' + carDetailsById);
    }


    //6. Создать Мапу где ключ это название двигателя, а значения это названия всех Автомобилей.
    public static void task_6() {

        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c ];
        System.debug('existEngines.size = ' + existEngines.size());


        Map<String, List<String>> engineCarMap = new Map<String, List<String>> ();

        for (Integer index = 0; index < existEngines.size(); index++) {

            Id engineId = existEngines[index].Id;

            List<Car__c> searchCars = [SELECT Id, Name, Brand__c, Year__c, Engine__c FROM Car__c WHERE Engine__c = :engineId];
            List<String> namesOfCars = new List<String>();

            for (Integer carIterator = 0; carIterator < searchCars.size(); carIterator++ ) {
                namesOfCars.add(searchCars[carIterator].Name);
            }

            engineCarMap.put(existEngines[index].Name, namesOfCars);
            System.debug('Cars with engine \'' + existEngines[index].Name + '\': '  + namesOfCars.size());
        }

        System.debug('engineCarMap.size() = ' + engineCarMap.size());
        System.debug('engineCarMap = ' + engineCarMap);

    }

    //7. Склонировать все данные в БД так, чтоб каждая запись повторялась(т.к.будет по два одинаковых двигателя, автомобиля и т.п.)
    public static void task_7() {

        //create duplicate Engines
        List<Engine__c> existEngines = [SELECT Name, Volume__c, Fuel_Type__c FROM Engine__c ];
        List<Engine__c> copyOfEngines = existEngines.deepClone();

        insert copyOfEngines;

        System.debug('Duplicate Engines created.');
        System.debug('existEngines.size = ' + existEngines.size());
        System.debug('existEngines[0].Id = ' + existEngines[0].Id);
        System.debug('copyOfEngines[0].Id = ' + copyOfEngines[0].Id);


        //create duplicate Cars
        List<Car__c> existCars = [SELECT Name, Brand__c, Year__c, Engine__c FROM Car__c];
        List<Car__c> copyOfCars = existCars.deepClone();

        insert copyOfCars;


        System.debug('Duplicate Cars created.');
        System.debug('existCars.size = ' + existCars.size());
        System.debug('existCars[0].Id = ' + existCars[0].Id);
        System.debug('copyOfCars[0].Id = ' + copyOfCars[0].Id);

        //create duplicate Details
        List<Detail__c> existDetails = [SELECT Name, Code__c, Engine__c FROM Detail__c];
        List<Detail__c> copyOfDetails = existDetails.deepClone();

        insert copyOfDetails;

        System.debug('Duplicate Details created.');
        System.debug('existDetails.size = ' + existDetails.size());
        System.debug('existDetails[0].Id = ' + existDetails[0].Id);
        System.debug('copyOfDetails[0].Id = ' + copyOfDetails[0].Id);


    }

    //8. Удалить все дубликаты с помощью повторной выборки данных из объектов ипутем их перебора. Используйте Мапу для сбора уникальных записей в БД.
    public static void task_8() {

        //Delete duplicate engines
        List<Engine__c> existEngines = [SELECT Id, Name, Volume__c, Fuel_Type__c FROM Engine__c ];

        Map<String, Engine__c> enginesMap = new Map<String, Engine__c>();
        List<Engine__c> enginesToDelete = new List<Engine__c>();

        for (Integer index = 0; index < existEngines.size(); index++) {

            String keyEngineParameters = existEngines[index].Name + existEngines[index].Volume__c + existEngines[index].Fuel_Type__c;

            if (enginesMap.containsKey(keyEngineParameters) == false) {
                enginesMap.put(keyEngineParameters, existEngines[index]);
            } else {
                enginesToDelete.add(existEngines[index]);
            }

        }

        delete enginesToDelete;

        System.debug('Duplicate Engines deleted.');
        System.debug('existEngines.size = ' + existEngines.size());
        System.debug('enginesMap.size() = ' + enginesMap.size());
        System.debug('enginesToDelete.size() = ' + enginesToDelete.size());


        //Delete duplicate cars
        List<Car__c> existCars = [SELECT Id, Name, Brand__c, Year__c FROM Car__c];

        Map<String, Car__c> carsMap = new Map<String, Car__c>();
        List<Car__c> carsToDelete = new List<Car__c>();

        for (Integer index = 0; index < existCars.size(); index++) {

            String keyCarParameters = existCars[index].Name + existCars[index].Brand__c + existCars[index].Year__c;

            if (carsMap.containsKey(keyCarParameters) == false) {
                carsMap.put(keyCarParameters, existCars[index]);
            } else {
                carsToDelete.add(existCars[index]);
            }

        }

        delete carsToDelete;

        System.debug('Duplicate Cars deleted.');
        System.debug('existCars.size = ' + existCars.size());
        System.debug('carsMap.size() = ' + carsMap.size());
        System.debug('carsToDelete.size() = ' + carsToDelete.size());



        //Delete duplicate details
        List<Detail__c> existDetails = [SELECT Id, Name, Code__c FROM Detail__c];

        Map<String, Detail__c> detailsMap = new Map<String, Detail__c>();
        List<Detail__c> detailsToDelete = new List<Detail__c>();

        for (Integer index = 0; index < existDetails.size(); index++) {

            String keyDetailParameters = existDetails[index].Name + existDetails[index].Code__c;

            if (detailsMap.containsKey(keyDetailParameters) == false) {
                detailsMap.put(keyDetailParameters, existDetails[index]);
            } else {
                detailsToDelete.add(existDetails[index]);
            }

        }

        delete detailsToDelete;

        System.debug('Duplicate Details deleted.');
        System.debug('existDetails.size = ' + existDetails.size());
        System.debug('detailsMap.size() = ' + detailsMap.size());
        System.debug('detailsToDelete.size() = ' + detailsToDelete.size());

    }


}